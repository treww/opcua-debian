#!/usr/bin/make -f

PARALLEL_JOBS=8

UAMAPPINGS_VERSION=0.2.0
OPCCORE_VERSION=0.1.3
OPCUA_CLIENT_VERSION=0.1.3
OPCUA_SERVER_VERSION=0.1.3
OPCUA_WSDL_VERSION=0.1.3
GMOCK_VERSION=1.7.0

UAMAPPINGS_GIT=https://github.com/treww/opcua-mappings.git
OPCCORE_GIT=https://github.com/treww/opcua-core.git
OPCUA_CLIENT_GIT=https://github.com/treww/opcua-client.git
OPCUA_SERVER_GIT=https://github.com/treww/opcua-server.git
OPCUA_WSDL_GIT=https://github.com/treww/opcua-wsdl.git

#UAMAPPINGS_GIT=/home/treww/work/opcua-mappings
#OPCCORE_GIT=/home/treww/work/opcua-core
#OPCUA_CLIENT_GIT=/home/treww/work/opcua-client
#OPCUA_SERVER_GIT=/home/treww/work/opcua-server
#OPCUA_WSDL_GIT=/home/treww/work/opcua-wsdl

UAMAPPINGS_PACKAGE_NAME=opcuamappings-$(UAMAPPINGS_VERSION)
UAMAPPINGS_DIR=$(UAMAPPINGS_PACKAGE_NAME)

OPCCORE_PACKAGE_NAME=opcuacore-$(OPCCORE_VERSION)
OPCCORE_DIR=$(OPCCORE_PACKAGE_NAME)

OPCUA_CLIENT_PACKAGE_NAME=opcuaclient-$(OPCUA_CLIENT_VERSION)
OPCUA_CLIENT_DIR=$(OPCUA_CLIENT_PACKAGE_NAME)

OPCUA_SERVER_PACKAGE_NAME=opcuaserver-$(OPCUA_SERVER_VERSION)
OPCUA_SERVER_DIR=$(OPCUA_SERVER_PACKAGE_NAME)

OPCUA_WSDL_PACKAGE_NAME=opcua-wsdl-$(OPCUA_WSDL_VERSION)
OPCUA_WSDL_DIR=$(OPCUA_WSDL_PACKAGE_NAME)


SRC_ROOT=${PWD}
CONFIGURE_OPTIONS="--with-uamappings=$(SRC_ROOT)/$(UAMAPPINGS_PACKAGE_NAME) \
--with-opccore=$(SRC_ROOT)/$(OPCCORE_PACKAGE_NAME) \
--with-opcuaserver=$(SRC_ROOT)/$(OPCUA_SERVER_PACKAGE_NAME) \
--with-libopcuaclient=$(SRC_ROOT)/$(OPCUA_CLIENT_PACKAGE_NAME) \
--with-gmock=$(SRC_ROOT)/$(gmock) \
--with-gtest=$(SRC_ROOT)/$(gmock)/gtest"

LD_LIBRARY_PATH="$(SRC_ROOT)/$(UAMAPPINGS_DIR)/.libs:$(SRC_ROOT)/$(OPCCORE_DIR)/.libs:$(SRC_ROOT)/$(OPCUA_CLIENT_DIR)/.libs:$(SRC_ROOT)/$(OPCUA_SERVER_DIR)/.libs"
DPKG_BUILDPACKAGE_OPTS=-j$(PARALLEL_JOBS)

REPREPRO_CMD=reprepro --basedir=repo

.PHONY: uamappings opccore client server wsdl initialize_repository gmock

all: client server wsdl

uamappings : $(UAMAPPINGS_PACKAGE_NAME) 
opccore : $(OPCCORE_PACKAGE_NAME)
client : $(OPCUA_CLIENT_PACKAGE_NAME)
server : $(OPCUA_SERVER_PACKAGE_NAME)
wsdl: $(OPCUA_WSDL_PACKAGE_NAME)


$(UAMAPPINGS_PACKAGE_NAME): gmock
	DPKG_BUILDPACKAGE_OPTS="$(DPKG_BUILDPACKAGE_OPTS)" CONFIGURE_OPTIONS=$(CONFIGURE_OPTIONS) PACKAGE_DIR=$(UAMAPPINGS_DIR)   PACKAGE_GIT=$(UAMAPPINGS_GIT)   ./make_deb.sh 2>&1 | tee $(UAMAPPINGS_PACKAGE_NAME).log
	$(REPREPRO_CMD) remove opcua opcuamappings opcuamappings-dev || true
	$(REPREPRO_CMD) include opcua $(UAMAPPINGS_DIR)/*.changes

$(OPCCORE_PACKAGE_NAME): $(UAMAPPINGS_PACKAGE_NAME)
	DPKG_BUILDPACKAGE_OPTS="$(DPKG_BUILDPACKAGE_OPTS)" CONFIGURE_OPTIONS=$(CONFIGURE_OPTIONS) PACKAGE_DIR=$(OPCCORE_DIR)      PACKAGE_GIT=$(OPCCORE_GIT)      ./make_deb.sh 2>&1 | tee $(OPCCORE_PACKAGE_NAME).log
	$(REPREPRO_CMD) remove opcua libopcuacore libopcuacore-dev || true
	$(REPREPRO_CMD) include opcua $(OPCCORE_DIR)/*.changes

$(OPCUA_CLIENT_PACKAGE_NAME): $(OPCCORE_PACKAGE_NAME)
	DPKG_BUILDPACKAGE_OPTS="$(DPKG_BUILDPACKAGE_OPTS)" LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) CONFIGURE_OPTIONS=$(CONFIGURE_OPTIONS) PACKAGE_DIR=$(OPCUA_CLIENT_DIR) PACKAGE_GIT=$(OPCUA_CLIENT_GIT) ./make_deb.sh 2>&1 | tee $(OPCUA_CLIENT_PACKAGE_NAME).log
	$(REPREPRO_CMD) remove opcua opcuaclient opcuaclient-dev || true
	$(REPREPRO_CMD) include opcua $(OPCUA_CLIENT_DIR)/*.changes

$(OPCUA_SERVER_PACKAGE_NAME): $(OPCUA_CLIENT_PACKAGE_NAME)
	DPKG_BUILDPACKAGE_OPTS="$(DPKG_BUILDPACKAGE_OPTS)" LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) CONFIGURE_OPTIONS=$(CONFIGURE_OPTIONS) PACKAGE_DIR=$(OPCUA_SERVER_DIR) PACKAGE_GIT=$(OPCUA_SERVER_GIT) ./make_deb.sh 2>&1 | tee $(OPCUA_SERVER_PACKAGE_NAME).log
	$(REPREPRO_CMD) remove opcua opcuaserver opcuaserver-dev opcuaserver-doc || true
	$(REPREPRO_CMD) include opcua $(OPCUA_SERVER_DIR)/*.changes

$(OPCUA_WSDL_PACKAGE_NAME): $(OPCUA_CLIENT_PACKAGE_NAME) $(OPCUA_SERVER_PACKAGE_NAME)
	DPKG_BUILDPACKAGE_OPTS="$(DPKG_BUILDPACKAGE_OPTS)" LD_LIBRARY_PATH=$(LD_LIBRARY_PATH) CONFIGURE_OPTIONS=$(CONFIGURE_OPTIONS) PACKAGE_DIR=$(OPCUA_WSDL_DIR)   PACKAGE_GIT=$(OPCUA_WSDL_GIT)   ./make_deb.sh 2>&1 | tee $(OPCUA_WSDL_PACKAGE_NAME).log
	$(REPREPRO_CMD) remove opcua opcua-wsdl-common opcua-wsdl-server opcua-wsdl-client || true
	$(REPREPRO_CMD) include opcua $(OPCUA_WSDL_DIR)/*.changes

gmock=gmock-$(GMOCK_VERSION)

$(gmock).zip:
	wget http://googlemock.googlecode.com/files/gmock-1.7.0.zip

$(gmock): $(gmock).zip
	unzip $(gmock).zip
	cd $(gmock) && ./configure --without-pthreads
	$(MAKE) -C $(gmock)

gmock: $(gmock)

initialize_repository:
	rm -rvf repo/db repo/dists repo/pool
	$(REPREPRO_CMD) export
	$(REPREPRO_CMD) createsymlinks

