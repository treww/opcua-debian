#!/bin/sh
ROOT_DIR=$PWD
git clone ${PACKAGE_GIT} ${PACKAGE_DIR}
cd ${PACKAGE_DIR}
echo LD_LIBRARY_PATH=${LD_LIBRARY_PATH}
./configure ${CONFIGURE_OPTIONS} && make -j8 check && make dist-deb

